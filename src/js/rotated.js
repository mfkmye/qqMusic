(function ($, root) {
    function Rotated() {
        this.deg = 0;
        this.timer = null;
    }
    Rotated.prototype = {
        startRotated: function () {
            this.timer = setInterval(() => {
                this.deg += 2;
                $('.img-wrapper').css({
                    'transform': 'rotatez(' + this.deg + 'deg)',
                    'transition': 'all .2s ease-in'
                })
            }, 200);
        },
        stopRotated: function () {
            clearInterval(this.timer);
        },
        reseatRotated: function () {
            this.stopRotated();
            this.deg = 0;
            $('.img-wrapper').css({
                'transform': 'rotatez(' + this.deg + 'deg)',
                'transition': 'none'
            });
        }
    }
    root.Rotated = Rotated;
}(window.jQuery, window.player || (window.player = {})))