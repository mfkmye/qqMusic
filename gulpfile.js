var gulp = require('gulp');

// 压缩html插件 : gulp-htmlclean
var htmlClean = require('gulp-htmlclean');
// 压缩图片插件 : gulp-imagemin
var imageMin = require('gulp-imagemin');
// 压缩js插件 : gulp-uglify
var uglify = require('gulp-uglify');
// 去掉调试语句 : 
var debug = require('gulp-strip-debug');
// less转css插件 : gulp-less
var less = require('gulp-less');
// 压缩css插件 : gulp-clean-css
var cleanCss = require('gulp-clean-css');
// css前缀插件 : gulp-postcss  autoprefixer(注:不是gulp-autoprefixer , 两者用法不一样)
var postCss = require('gulp-postcss');
var autopreFixer = require('autoprefixer');
// 开启服务器 : gulp-connect
var connect = require("gulp-connect");
// 报错信息
var gutil = require("gulp-util");
// babel ES6 -> ES5
var babel = require("gulp-babel");

var folder = {
    src: "src/",
    dist: "dist/"
}

// 命令端执行以下命令，设置环境变量:
//      export NODE_EVN = development
// var devMod = process.env.NODE_ENV == "development";
// console.log(devMod);
var devMod = true;

function htmlTask(cb) {
    console.log(folder.src);
    var task = gulp.src(folder.src + "index.html").pipe(connect.reload()); //添加刷新
    if (!devMod) {
        task.pipe(htmlClean()) // 使用htmlClean 插件
    }
    task.pipe(gulp.dest(folder.dist)); // 输出文件
    cb();
}

function cssTask(cb) {
    var task = gulp.src(folder.src + "css/*").pipe(connect.reload()).pipe(less()).pipe(postCss([autopreFixer({
        'overrideBrowserslist': ['last 4 versions']
    })]));
    if (!devMod) {
        task.pipe(cleanCss())
    }
    task.pipe(gulp.dest(folder.dist + "css/"));
    cb();
}

function jsTask(cb) {
    var task = gulp.src(folder.src + "js/*")
        .pipe(connect.reload())
        .pipe(babel({
            presets: ['@babel/env']
        }))
    if (!devMod) {
        task.pipe(debug()).pipe(uglify())
    }
    task.on('error', function (err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
        })
        .pipe(gulp.dest(folder.dist + "js/"));
    cb();
}

function imgTask(cb) {
    gulp.src(folder.src + 'img/*')
        .pipe(imageMin())
        .pipe(gulp.dest(folder.dist + "img/"));
    cb();
}

function copySource(cb) {
    gulp.src(folder.src + 'source/*').pipe(gulp.dest(folder.dist + "source/"));
    gulp.src(folder.src + 'mock/*').pipe(gulp.dest(folder.dist + "mock/"));
    cb();
}

// 监听文件变化
function watchTask(cb) {
    gulp.watch(folder.src + 'index.html', htmlTask);
    gulp.watch(folder.src + 'css/*', cssTask);
    gulp.watch(folder.src + 'js/*', jsTask);
    cb();
}

function serverTask(cb) {
    connect.server({
        port: "8888",
        livereload: true // 开启自动刷新
    });
    cb();
}

exports.default = gulp.series(htmlTask, cssTask, jsTask, imgTask, watchTask, serverTask, copySource);